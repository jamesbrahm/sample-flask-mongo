import flask_mongoengine, flask_login, flask_security

db = flask_mongoengine.MongoEngine()

class Role(db.Document, flask_security.RoleMixin):
    name = db.StringField(max_length=100, unique=True)
    description = db.StringField(max_length=255)


class User(db.Document, flask_login.UserMixin):
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])
