import os

import flask, flask_mongoengine, flask_security

from app.views import web_view
from app.models import *


# Create the app
app = flask.Flask(__name__)
app.debug = True
app.config['DEBUG'] = True
app.config["SECRET_KEY"] = "bf35468b67b90901b44f2e38ee2885c6"

# MongoDB Config
app.config["MONGODB_HOST"] = "localhost"
app.config["MONGODB_DB"] = 'localdb'
app.config["MONGODB_PORT"] = 27017
db.init_app(app)

# Security config settings
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_CONFIRMABLE'] = False
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_PASSWORD_HASH'] = 'plaintext' # PLEASE OH PLEASE CHANGE THIS BEFORE PRODUCTION
#app.config['SECURITY_PASSWORD_SALT'] = 

# Setup Flask-Security
user_datastore = flask_security.MongoEngineUserDatastore(db, User, Role)
security = flask_security.Security(app, user_datastore)


app.register_blueprint(web_view)


if __name__ == "__main__":
    app.run(debug=True)
