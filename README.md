# Sample Flask/Mongo Site

This is a very basic web app using Flask and mongodb.  Ultimately, it should include authentication and an easily deployable setup.

## About

## Installation

```
git clone https://gitlab.com/novabrahm/sample-flask-mongo.git
cd sample-flask-mongo

# Install system dependencies
sudo apt install mongodb

# Create virtual environment for Python
virtualenv -p python3 venv

# Activate the virtual environment
source venv/bin/activate

# Install Python dependencies
pip install -r requirements.txt

# Run the uwsgi server
./run.sh

# Defaults to listening on 8080
```

